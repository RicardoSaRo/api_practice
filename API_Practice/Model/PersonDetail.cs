﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API_Practice.Model
{
    public class PersonDetail
    {
        [Key]
        public int ID { get; set; }

        public String PersonCity { get; set; }

        public DateTime Birthday { get; set; }

        [ForeignKey("Person")]
        public int PersonID { get; set; }

        public Person Person { get; set; }

    }
}
