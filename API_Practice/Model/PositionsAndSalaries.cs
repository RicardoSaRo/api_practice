﻿namespace API_Practice.Model
{
    public class PositionsAndSalaries
    {
        public List<Position> PositionList{ get; set; }

        public List<Salary> SalaryList { get; set; }
    }
}
