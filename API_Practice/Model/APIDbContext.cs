﻿using Microsoft.EntityFrameworkCore;

namespace API_Practice.Model
{
    public class APIDbContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }

        public DbSet<Position> Positions { get; set; }

        public DbSet<Salary> Salarys { get; set; }

        public DbSet<Department> Departments { get; set; }

        public DbSet<PersonDetail> PersonDetail { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source = DESKTOP-TJ2779T\SQLEXPRESS; Initial Catalog = APIDB; Persist Security Info = True; User ID = FSP; Password = M@nt1c0r3");
        }
    }
}
