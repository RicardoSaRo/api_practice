﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace API_Practice.Model
{
    public class Department
    {
        public Department()
        {
            Positions = new HashSet<Position>();
        }

        [Key]
        public int DepartmentID { get; set; }

        [Required]
        public string DepartmentName { get; set; }

        public virtual ICollection<Position> Positions { get; set; }
    }
}
