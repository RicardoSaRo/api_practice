﻿using System.ComponentModel.DataAnnotations;

namespace API_Practice.Model
{
    public class Salary
    {
        public Salary()
        {
            Persons = new HashSet<Person>();
        }

        [Key]
        public int SalaryID { get; set; }

        public int Amount { get; set; }

        public virtual ICollection<Person> Persons { get; set; }
    }
}
