﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API_Practice.Model
{
    public class Person
    {
        [Key]
        public int ID { get; set; }
        
        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public int Age { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
        
        public string Address { get; set; }

        [ForeignKey("Position")]
        public int PositionID { get; set; }

        public Position? Position { get; set; }

        [ForeignKey("Salary")]
        public int SalaryID { get; set; }

        public Salary? Salary { get; set; }

        public virtual PersonDetail? PersonDetail { get; set; }
    }
}
