﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API_Practice.Model
{
    public class Position
    {
        public Position()
        {
            Persons = new HashSet<Person>();
        }

        [Key]
        public int PositionID { get; set; }

        [Required]
        public string PositionName { get; set; }

        public virtual ICollection<Person> Persons { get; set; }

        [ForeignKey("Department")]
        public int DepartmentID { get; set; }

        public Department Department { get; set; }
    }
}
