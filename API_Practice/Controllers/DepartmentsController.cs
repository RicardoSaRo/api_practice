﻿using API_Practice.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_Practice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        [HttpGet] //--> If nothing is defined, this is the Default
        public IActionResult Get()
        {
            var db = new APIDbContext();
            var list = db.Departments.ToList();
            return Ok(list); //--> For 200ok response and sending as well the list
        }

        [HttpGet("{Id}")]
        public IActionResult Get(int Id)
        {
            var db = new APIDbContext();
            Department department = db.Departments.Find(Id);
            if (department == null)
                return NotFound();
            else
                return Ok(department);
        }

        [HttpPost]
        public IActionResult AddDepartment(Department department)
        {
            if (ModelState.IsValid)
            {
                var db = new APIDbContext();
                db.Add(department);
                db.SaveChanges();
                return Created("", department);
            }
            else
                return BadRequest();
        }

        [HttpPut]
        public IActionResult UpdateDepartment(Department department)
        {
            if (ModelState.IsValid)
            {
                var db = new APIDbContext();
                Department dep = db.Departments.Find(department.DepartmentID);
                dep.DepartmentName = department.DepartmentName;
                db.SaveChanges();
                return NoContent();
            }
            else
                return BadRequest();

        }

        [HttpDelete("{Id}")]
        public IActionResult DeleteDepartment(int Id)
        {
                var db = new APIDbContext();
                Department dep = db.Departments.Find(Id);
                if (dep == null)
                    return NotFound();
                else
                {
                    db.Departments.Remove(dep);
                    db.SaveChanges();
                    return NoContent();
                }
        }
    }
}
