﻿using API_Practice.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API_Practice.Controllers
{
    [EnableCors]
    [Route("api/[controller]")]
    [ApiController]
    public class PersonsController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            var db = new APIDbContext();
            var list = db.Persons.Include(x => x.Salary).Include(x => x.Position).
                    ThenInclude(x => x.Department).
                    Select(x => new PersonALL()
                    {
                        ID = x.ID,
                        Name = x.Name,
                        PositionName = x.Position.PositionName,
                        Salary = x.Salary.Amount,
                        PersonCity = x.PersonDetail.PersonCity,
                        DepartmentName = x.Position.Department.DepartmentName
                    }).ToList();
            return Ok(list);
        }

        [HttpGet("{Id}")]
        public IActionResult Get(int Id)
        {
            var db = new APIDbContext();
            var person = db.Persons.FirstOrDefault(x => x.ID == Id);
            if (person == null)
                return NotFound();
            else
                return Ok(person);
        }

        [HttpGet("positionsandsalaries")]
        public IActionResult GetPositionsAndSalaries()
        {
            var db = new APIDbContext();
            PositionsAndSalaries lists = new PositionsAndSalaries();
            lists.PositionList = db.Positions.ToList();
            lists.SalaryList = db.Salarys.ToList();
            return Ok(lists);
        }

        [HttpPost]
        public IActionResult Post(Person person)
        {
            if (ModelState.IsValid)
            {
                var db = new APIDbContext();
                db.Persons.Add(person);
                db.SaveChanges();
                return Created("", person);
            }
            else
                return BadRequest();
        }

        [HttpPost("uploadfile")]
        public async Task<IActionResult> UploadFile([FromForm]IFormFile file)
        {
            string filemane = Guid.NewGuid().ToString() + file.FileName;
            var filepath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/PersonFiles/" + filemane);
            FileStream stream = new FileStream(filepath, FileMode.Create);
            await file.CopyToAsync(stream);
            return Created("", null);
        }

        [HttpPut]
        public IActionResult UpdatePerson(Person person)
        {
            if (ModelState.IsValid)
            {
                var db = new APIDbContext();
                Person uperson = db.Persons.Find(person.ID);
                uperson.Address = person.Address;
                uperson.Age = person.Age;
                uperson.Email = person.Email;
                uperson.Name = person.Name;
                uperson.Surname = person.Surname;
                uperson.Password = person.Password;
                uperson.PositionID = person.PositionID;
                uperson.SalaryID = person.SalaryID;
                db.SaveChanges();
                return NoContent();
            }
            else
                return BadRequest();
        }
    }
}
