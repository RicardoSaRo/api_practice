﻿using API_Practice.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_Practice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalariesController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            var db = new APIDbContext();
            var list = db.Salarys.ToList();
            return Ok(list);
        }

        [HttpPost]
        public IActionResult Add(Salary salary)
        {
            if (ModelState.IsValid)
            {
                var db = new APIDbContext();
                db.Add(salary);
                db.SaveChanges();
                return Created("", salary);
            }
            else
                return BadRequest();
        }
        

        [HttpDelete("{Id}")]
        public IActionResult Delete(int Id)
        {
            var db = new APIDbContext();
            Salary salary = db.Salarys.Find(Id);
            if(salary == null)
            {
                return NotFound();
            }
            else
            {
                db.Salarys.Remove(salary);
                db.SaveChanges();
                return NoContent();
            }
        }
    }
}
